/**
 * Dutch Blitz Card Deck
 */

package com.dutchblitz;

import android.content.Context;
import android.view.View;

import java.util.Arrays;
import java.util.Random;


public class Deck {
	
	private Random rand = new Random();
	private Card[] deck;
	private int topCardIndex;
	
	public Deck(Context con, String deckColor) {
		int numCardsInDeck = Card.VALID_COLORS.length * (Card.MAX_VALUE - Card.MIN_VALUE + 1);
		// Initialize the deck array with a size of the most possible cards
		deck = new Card[numCardsInDeck];
		topCardIndex = numCardsInDeck - 1;
		if (!Arrays.asList(Card.VALID_COLORS).contains(deckColor)) {
			// The color selected was invalid, reset to default
			deckColor = Card.RED;
		}

		// Create a card for every possible suit-value combination
		for (String suitColor:Card.VALID_COLORS) {
			for (int i = 0; i < (Card.MAX_VALUE - Card.MIN_VALUE + 1); i++) {
				// Cards in the deck are initialized
				// Note: they are all face down
				Card card = new Card(con, deckColor, suitColor, i + Card.MIN_VALUE, false);
				// Find the index of the cards
				int colorIndex = Arrays.asList(Card.VALID_COLORS).indexOf(suitColor);
				// Save the card in the deck array
				deck[colorIndex * (Card.MAX_VALUE - Card.MIN_VALUE + 1) + i] = card;
			}			
		}
	}
	
	public void shuffle() {
		// Create a duplicate deck to copy to
		Card[] newDeck = new Card[deck.length];
		boolean validSpot = false;
		// Index to determine where we are in the NEW deck
		int newIndex;
		for (int i = 0; i < deck.length; i++) {
			while (!validSpot) {
				newIndex = rand.nextInt(deck.length);
				if (newDeck[newIndex] == null) {
					validSpot = true;
					newDeck[newIndex] = deck[i];
				}
			}
			validSpot = false;
		}
		deck = newDeck;
	}
	
	public Card[] cards() {
		return deck;
	}
	
	public Card getCardFromImageView(View v) {
		// This will return the Card an ImageView belongs to
		Card cardFound = null;
		for (Card card:deck) {
			if (card.equals(v)) {
				cardFound = card;
			}
		}
		return cardFound;
	}
	
	public void moveTo(float x, float y) {
		// This moves the entire deck to x, y
		for (Card card:deck) {
			card.moveTo(x,  y);
		}
	}
	
	public Card getTopCard() {
		if (topCardIndex >= 0) {
			return deck[topCardIndex--];
		} else {
			return null;
		}
	}
	
	public int indexOf(Card card) {
		int index = -1;
		int currPos = 0;
		for (Card c:deck) {
			if (c.equals(card)) {
				index = currPos;
			}
			currPos ++;			
		}
		return index;
	}
	
	public Card getCardAt(int index) {
		return deck[index];
	}
	
	public String toString() {
		String str = "";
		for (Card card:deck) {
			str += card.toString() + ";";
		}	
		return str;
	}
}