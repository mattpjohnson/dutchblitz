/**
 * Dutch Blitz card game
 * For rules see www.dutchblitz.com/rules.htm
 */

package com.dutchblitz;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.Toast;

public class CardGame extends Activity {
	
	private static final int ANIMATION_DURATION = 300;
	private static final int NUM_CARDS_IN_BLITZ_PILE = 10;
	private static final int NUM_POST_PILES_PER_PLAYER = 3;
	private static final int TABLE_NUM_DUTCH_PILES_TALL = 4;
	private static final int TABLE_NUM_DUTCH_PILES_WIDE = 4;
	// The number of cards flipped at a time in the wood pile
	private static final int NUM_CARDS_FLIPPED_IN_WOOD_PILE = 1;
	// How much each wood card is offset to the right of the previous one
	private static final float WOOD_CARDS_FACE_UP_OFFSET = 10;
	private static final float BLITZ_CARDS_OFFSET = 0.5f;

	private String playerName;
	// The color of the player's deck
	private String playerColor;
	// The dimensions of the screen
	private int screenWidth;
	private int screenHeight;	
	// Where the Post and Blitz piles will go
	private int bottomPilesYPos;
	private int blitzPileXPos;
	// The y position of the wood pile [the pile the user flips in their hand (top of screen)]
	private int woodPileYPos;
	private int woodPileXPos;
	// The position of a card before it moves in case it must be returned
	private float initialX;
	private float initialY;
	// Has the game been won?
	private boolean gameHasBeenWon;
	// Only one card can be moving per player at a time according to the rules
	private boolean cardMoving;
	// Track which card is currently moving
	private View movingCard;
	// I have to do a few things after the focus has been changed, but I only want to do it once
	private boolean focusChangedAlready;
	// This FrameLayout represents the table
	private FrameLayout flCardTable;
	// Create a new Deck to deal cards to the player
	private Deck deck;
	// This is the height that each card had available
	private int heightAvailPerCard;
	// This is the width that each card will have available in the players Dutch piles
	private int widthAvailPerCardInDutchPiles;
	private int widthAvailPerCardInHand;
	// Keep track of the cards in the players blitz pile
	private ArrayList<Card> blitzPile;
	// keep track of the cards in the player's wood pile
	private ArrayList<Card> woodPileFaceUp;
	private ArrayList<Card> woodPileFaceDown;
	// Keep track of the dutch piles in the center of the table and the cards in them
	private ArrayList<ArrayList<Card>> dutchPiles = new ArrayList<ArrayList<Card>>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.card_table_test);

		flCardTable = (FrameLayout) findViewById(R.id.flCardTable);
		focusChangedAlready = false;
		blitzPile = new ArrayList<Card>();
		woodPileFaceUp = new ArrayList<Card>();
		woodPileFaceDown = new ArrayList<Card>();
		gameHasBeenWon = false;
	}

    /**
     * This is called after onCreate()
     */
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		
		// Make sure this hasn't already been run
		if (focusChangedAlready) {
			return;
		}
		
		focusChangedAlready = true;
		// Get the dimensions of the visible screen
		screenHeight = flCardTable.getMeasuredHeight();
		screenWidth = flCardTable.getMeasuredWidth();
		// The amount of space each card in the hand will get
		widthAvailPerCardInHand = screenWidth / (NUM_POST_PILES_PER_PLAYER + 3);
		widthAvailPerCardInDutchPiles = screenWidth / TABLE_NUM_DUTCH_PILES_WIDE;
		heightAvailPerCard = (screenHeight) / (TABLE_NUM_DUTCH_PILES_TALL + 2);
		// Determine where the deck's y coordinate should be
		bottomPilesYPos = (heightAvailPerCard * (TABLE_NUM_DUTCH_PILES_TALL + 1)) + ((heightAvailPerCard - Card.HEIGHT) / 2);
		//woodPileYPos = (heightAvailPerCard - Card.HEIGHT) - ((heightAvailPerCard - Card.HEIGHT) / 2);
		blitzPileXPos = (widthAvailPerCardInHand * NUM_POST_PILES_PER_PLAYER) + ((widthAvailPerCardInHand - Card.WIDTH) / 2);
		woodPileYPos = screenHeight - (int) ((float) Card.HEIGHT * 0.75f);
		woodPileXPos = screenWidth - (int) ((float) Card.WIDTH * 0.75f);
		
		playerName = askPlayerName();
		//getPlayerChooseColor();
		playerColor = Card.GREEN;
		dealCards();
	}  
	
	public String askPlayerName() {
		return "Matt";
	}
	
	public void getPlayerChooseColor() {
		Card card;
		int counter = 0;
		for (String deckColor:Card.VALID_COLORS) {
			card = new Card(this, deckColor, Card.RED, Card.MAX_VALUE, false);
			card.addToLayout(flCardTable);
			card.moveTo((screenWidth / 2) + ((counter % 2) * Card.WIDTH), (screenHeight / 2) + (counter / 2) * Card.HEIGHT);
			card.setOnTouchListener(new OnTouchListener() {
				public boolean onTouch(View view, MotionEvent event) {
					colorChosen(view);
					return true;
				}
			});
			counter++;
		}
	}

    /*
    * Called when the user has chosen the color of their deck
    */
	public void colorChosen(View view) {
		flCardTable.removeAllViews();
		dealCards();
	}
	
	public void dealCards() {
		
		Card card = null;
		deck = new Deck(this, playerColor);
		deck.shuffle();
		deck.moveTo((screenWidth - Card.WIDTH) / 2, (screenHeight - Card.HEIGHT) / 3);
		
		// Add each card to the table
		for (Card c:deck.cards()) {
			c.addToLayout(flCardTable);
		}
		
		// The number of Dutch piles the table can hold
		int numDutchPilesOnTable = TABLE_NUM_DUTCH_PILES_TALL * TABLE_NUM_DUTCH_PILES_WIDE;
		
		// Create an ArrayList for each dutch pile so we can track the cards in them
		for (int i = 0; i < numDutchPilesOnTable; i++) {
			dutchPiles.add(new ArrayList<Card>());
		}
		
		// There is not currently a card moving
		cardMoving = false;
		movingCard = null;
		
		/** 
		 * Deal cards to the players post pile positions
		 */
		
		for (int i = 0; i < NUM_POST_PILES_PER_PLAYER; i++) {
			// Deal the next card
			card = deck.getTopCard();
			// Put the card on top
			card.bringToFront();
			// Flip the card up so we can see it
			card.flipCard(true);
			// Set an OnTouchListener for the card
			card.setOnTouchListener(new OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					playableCardWasTouched(v, event, "postpilecard");
					return true;
				}
			});
			// The x position of where the card will go
			int x = (widthAvailPerCardInHand * i) + (widthAvailPerCardInHand / 2) - (Card.WIDTH / 2);
			// Animate the card being dealt
			card.animateTo(x, bottomPilesYPos, ANIMATION_DURATION);
		}
		
		/**
		 *  Deal cards to the Blitz pile
		 */
		
		while ((blitzPile.size() < NUM_CARDS_IN_BLITZ_PILE) && ((card = deck.getTopCard()) != null)) {
			card.bringToFront();
			// Animate the card being dealt
			card.animateTo(blitzPileXPos - (blitzPile.size() * BLITZ_CARDS_OFFSET), bottomPilesYPos + (blitzPile.size() * BLITZ_CARDS_OFFSET), ANIMATION_DURATION);
			blitzPile.add(card);
		}
		Card blitzTopCard = blitzPile.get(blitzPile.size() - 1);
		blitzTopCard.flipCard(true);
		blitzTopCard.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				playableCardWasTouched(v, event, "postpilecard");
				return true;
			}
		});
		
		/**
		 *  Deal the remainder of the cards to the wood pile
		 */
		
		while ((card = deck.getTopCard()) != null) {
			// Bring the card to the top
			card.bringToFront();
			// Add the card to the woodPile ArrayList
			woodPileFaceDown.add(card);
			// Move the card's ImageView to the wood pile
			card.moveTo(woodPileXPos, woodPileYPos);
		}
		Card woodPileTopCard = woodPileFaceDown.get(woodPileFaceDown.size() - 1);
		// Set an OnTouchListener on the top card
		woodPileTopCard.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View view, MotionEvent event) {
				woodPileWasTouched(view, event);
				return true;
			}
		});
	}
	
	private boolean cardInValidSpot(Card c, int index) {
		float cardY = c.getY();
		
		if (cardY < (heightAvailPerCard / 2)) {
			// The card is too high
			return false;
		} else if ((cardY + Card.HEIGHT - (heightAvailPerCard / 2)) > (heightAvailPerCard * (TABLE_NUM_DUTCH_PILES_TALL + 1))) {
			// The card is too low
			return false;
		} 
		
		// The card is on top of a dutch pile
		// Is it a valid dutch pile?

		ArrayList<Card> dutchPile = dutchPiles.get(index);
		
		if (c.getCardValue() == 1) {
			// The card is a 1 card, it can only be placed on an empty spot
			if (dutchPile.size() == 0) {
				// This dutch pile is empty 
				return true;
			} else {
				// The pile is not empty
				return false;
			}
		} else if (dutchPile.size() <= 0) {
			// The dutch pile is empty, so it is not valid
			return false;
		}
		
		// The cards value is greater than 1 and the dutch pile is not empty
		// Is the card on top 1 less than the current card?
		
		// Get a reference to the card on top
		Card dutchPileTopCard = dutchPile.get(dutchPile.size() - 1);
		// Does the top cards suit color match the new cards suit color?
		boolean isCorrectSuit = dutchPileTopCard.getSuitColor().equals(c.getSuitColor());
		// Is the new cards value 1 greater than the top card?
		boolean isCorrectValue = dutchPileTopCard.getCardValue() == (c.getCardValue() - 1);
		
		if (isCorrectSuit && isCorrectValue) {
			return true;
		} else {
			return false;
		}
	}
	
	private void playableCardWasTouched(View v, MotionEvent event, String cardType) {
		v.bringToFront();
		// Get the Card that the View belongs to
		Card card = deck.getCardFromImageView(v);
		// The card shouldn't be touched if it's face down or the game has already been won
		if (!card.isFaceUp() || gameHasBeenWon) {
			return;
		}
		
		if (event.getAction() == MotionEvent.ACTION_MOVE) {
			// Make sure there isn't a different card already moving
			if (!cardMoving || (movingCard.equals(v))) {
				if (!cardMoving) {
					// Track where the card was initially in case it has to be put back
					initialX = v.getX();
					initialY = v.getY();
					// There is a moving card
					cardMoving = true;
					// Set this variable to the current View so we can make sure only 1 card moves at a time
					movingCard = v;
				} else {
					// Move the center of the card to where the users finger is
					card.moveTo(event.getRawX() - (Card.WIDTH / 2), event.getRawY() - Card.HEIGHT);
				}
			}
		} else if (cardMoving) {
			/**
			* The card has finished moving
			* Check if the card is in a valid spot
			*/
			
			cardMoving = false;
			movingCard = null;
			// Track where the card is currently
			float cardX = card.getX();
			float cardY = card.getY();
			// Verify the card is completely on the table
			if (cardX < 0) {
				// The card is off the left side of the screen
				cardX = 0;
			} else if ((cardX + Card.WIDTH) > screenWidth) {
				// The card is off the right side of the screen
				cardX = screenWidth - Card.WIDTH;
			}
			
			if (cardY < heightAvailPerCard) {
				// The card is too high, move it down to an appropriate position
				cardY = heightAvailPerCard;
			} else if (cardY > (screenHeight - heightAvailPerCard) - Card.HEIGHT) {
				// The card is too low, move it up to an appropriate position
				cardY = screenHeight - heightAvailPerCard - Card.HEIGHT;
			}
			
			// Move the card to the dutch pile it was placed on
			
			// Coordinates of where the card is on the table if it was a grid
			int xGridPos = (int) ((cardX + (Card.WIDTH / 2)) / widthAvailPerCardInDutchPiles);
			int yGridPos = ((int) ((cardY + (Card.HEIGHT / 2)) / heightAvailPerCard) - 1);
			// Determine the index of the dutch pile it's one to add it to that piles ArrayList
			int gridPosIndex = (yGridPos * TABLE_NUM_DUTCH_PILES_WIDE) + xGridPos;
			boolean cardInValidSpot = cardInValidSpot(card, gridPosIndex);
			if (cardInValidSpot) {

				card.randomRotate(Card.RANDOM_ROTATE_AMOUNT);

				// Coordinates of where the card will need to go to be in a valid dutch pile
				float xPos = (xGridPos * widthAvailPerCardInDutchPiles) + ((widthAvailPerCardInDutchPiles - Card.WIDTH) / 2);
				float yPos = ((yGridPos * heightAvailPerCard) + (heightAvailPerCard) + (heightAvailPerCard - Card.HEIGHT) / 2);
				// Adjust the card so it's centered on it's dutch pile
				card.animateTo(xPos , yPos, ANIMATION_DURATION);
				ArrayList<Card> dutchPile = dutchPiles.get(gridPosIndex);
				// Add the card to the stack at xGridPos, xYPos
				dutchPile.add(card);
				// Set the onTouchListener so the card won't do anything when touched
				card.resetOnTouchListener();
				
				if (cardType.equals("postpilecard")) {
					// Deal the next card to take up the cards spot
					Card nextCard = blitzPile.get(blitzPile.size() - 1);
					nextCard.animateTo(initialX, initialY, ANIMATION_DURATION);
					// The card is no longer in the blitz pile
					blitzPile.remove(nextCard);
					// Flip the card so it's face up
					nextCard.flipCard(true);
					// Set an OnTouchListener for the new card
					nextCard.setOnTouchListener(new OnTouchListener() {
						@Override
						public boolean onTouch(View v, MotionEvent event) {
							playableCardWasTouched(v, event, "postpilecard");
							return true;
						}
					});
					
					if (blitzPile.size() == 0) {
						// All of the cards in the BlitzPile have been dealt, so the player wins
						gameWon(playerName, playerColor);
						return;
					}
					
					Card blitzTopCard = blitzPile.get(blitzPile.size() - 1);
					blitzTopCard.flipCard(true);
					blitzTopCard.setOnTouchListener(new OnTouchListener() {
						@Override
						public boolean onTouch(View v, MotionEvent event) {
							playableCardWasTouched(v, event, "postpilecard");
							return true;
						}
					});
				} else if (cardType.equals("woodpilecard")) {
					woodPileFaceUp.remove(card);
					if (woodPileFaceUp.size() > 0) {
						Card nextCard = woodPileFaceUp.get(woodPileFaceUp.size() - 1);
						nextCard.setOnTouchListener(new OnTouchListener() {
							public boolean onTouch(View view, MotionEvent event) {
								playableCardWasTouched(view, event, "woodpilecard");
								return true;
							}
						});
					}
				}
			} else {
				// The card is not in a valid spot
				// Move the card back to its initial position
				card.animateTo(initialX, initialY, ANIMATION_DURATION);
			}
		}
	}

	private void woodPileWasTouched(View view, MotionEvent event) {
		
		if (gameHasBeenWon) {
			return;
		}
		
		if (event.getAction() == MotionEvent.ACTION_MOVE) {
			if (!cardMoving || movingCard.equals(view)) {
				cardMoving = true;
				movingCard = deck.getCardFromImageView(view);
				float x = event.getRawX();
				float y = event.getRawY();
				
				for (int i = 0; i < NUM_CARDS_FLIPPED_IN_WOOD_PILE; i++) {
					if (woodPileFaceDown.size() >= (i + 1)) {
						Log.d("Debugging", "I think the problem is here...");
						Card card = woodPileFaceDown.get(woodPileFaceDown.size() - (i + 1));
						Log.d("Debugging", "That was apparently not the problem");
						card.moveTo(x + (WOOD_CARDS_FACE_UP_OFFSET * i) - (Card.WIDTH / 2), y - Card.HEIGHT);
					}
				}
			}
		} else if (cardMoving) {
			cardMoving = false;
			movingCard = null;
			for (int i = 0; i < NUM_CARDS_FLIPPED_IN_WOOD_PILE; i++) {
				if (woodPileFaceDown.size() > 0) {
					Card card = woodPileFaceDown.get(woodPileFaceDown.size() - 1);
					card.animateTo((widthAvailPerCardInHand * (NUM_POST_PILES_PER_PLAYER + 1)) + (widthAvailPerCardInHand - Card.WIDTH) / 2 + (WOOD_CARDS_FACE_UP_OFFSET * i), bottomPilesYPos, ANIMATION_DURATION);
					card.flipCard(true);
					card.bringToFront();
					card.resetOnTouchListener();
					woodPileFaceDown.remove(card);
					woodPileFaceUp.add(card);
				}
			}
			Card topCard = woodPileFaceUp.get(woodPileFaceUp.size() - 1);
			topCard.setOnTouchListener(new OnTouchListener() {
				public boolean onTouch(View view, MotionEvent event) {
					playableCardWasTouched(view, event, "woodpilecard");
					return true;
				}
			});
			if (woodPileFaceDown.size() <= 0) {
				for (int i = (woodPileFaceUp.size() - 1); i >= 0; i--) {
					Card card = woodPileFaceUp.get(i);
					card.flipCard(false);
					card.moveTo(woodPileXPos, woodPileYPos);
					woodPileFaceDown.add(woodPileFaceUp.get(i));
					woodPileFaceUp.remove(woodPileFaceUp.get(i));
					Log.d("Debugging", "Card moved");
				}
			}
			Card woodPileTopCard = woodPileFaceDown.get(woodPileFaceDown.size() - 1);			
			woodPileTopCard.setOnTouchListener(new OnTouchListener() {
				public boolean onTouch(View view, MotionEvent event) {
					woodPileWasTouched(view, event);
					return true;
				}
			});
			woodPileTopCard.bringToFront();
		}
	}

    /*
    * Alerts players that the game has won and stops gameplay
    */
	private void gameWon(String winningName, String winningDeckColor) {
		gameHasBeenWon = true;
		// Congratulate the player
		Toast.makeText(this, winningName + " (" + winningDeckColor + ") has won!", Toast.LENGTH_LONG).show();
	}
}