/**
 * This class represents a Dutch Blitz Card
 */

package com.dutchblitz;

import java.util.HashMap;
import java.util.Random;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;


public class Card extends ImageView {

	// Card Colors, these are valid for both suit and deck colors
	public static final String BLUE = "blue";
	public static final String GREEN = "green";
	public static final String RED = "red";
	public static final String YELLOW = "yellow";
	// Minimum and Maximum values for any card
	public static final int MIN_VALUE = 1;
	public static final int MAX_VALUE = 10;
	// The dimensions of the card
	public static final int HEIGHT = 150;
	public static final int WIDTH = 104;
	// The VALID_COLORS is valid for both the suit and deck colors
	public static final String[] VALID_COLORS = {BLUE, GREEN, RED, YELLOW};
	// Cards can be rotated randomly to make it look more realistic
	// This is rotation offset in degrees
	public static final int RANDOM_ROTATE_AMOUNT = 5; 
 
	
	// Track the suit (front) color of the card
	private String cardSuitColor;
	// Track the Deck color of the card
	private String cardDeckColor;
	// Track the value of the card
	private int cardValue;
	// Is the card face up?
	private boolean cardIsFaceUp;
	// This is used to randomly rotate the card for realism
	private Random rand;
	// This is used to animate the card moving
	private TranslateAnimation animation;
	// The res ID of each of the suits 1 cards
	private HashMap<String, Integer> minFrontImageIds = new HashMap<String, Integer>();
	private Bitmap frontImageBitmap;
	private Bitmap backImageBitmap;
	
	
	public Card(Context con) {
		this(con, RED, RED, MAX_VALUE, false);
	}
	
	public Card(Context con, String deckColor, String frontColor, int value, boolean faceUp) {

		super(con);
		
		if ((value < MIN_VALUE) || (value > MAX_VALUE)) {
			value = MAX_VALUE;
		}
		
		// Used to rotate the card randomly for realism
		rand = new Random();
		
		// Set handles for the cards values 
		cardSuitColor = frontColor;
		cardDeckColor = deckColor;
		cardValue = value;
		cardIsFaceUp = faceUp;
		
		// These are the raw IDs of the 1 cards of each color
		minFrontImageIds.put(BLUE, R.drawable.blue_01_card);
		minFrontImageIds.put(GREEN, R.drawable.green_01_card);
		minFrontImageIds.put(RED, R.drawable.red_01_card);
		minFrontImageIds.put(YELLOW, R.drawable.yellow_01_card);
		
		setImageBitmapsForCard();
		Bitmap cardImage = cardIsFaceUp ? frontImageBitmap : backImageBitmap;
		setImage(cardImage);
	}
	
	private void setImageBitmapsForCard() {
		// Updates the frontImageBitmap and backImageBitmap for the card
		// This is very expensive as far as CPU is concerned
		int frontImageId = minFrontImageIds.get(cardSuitColor) + (cardValue - 1);
		int backImageId = R.drawable.red_card_back;
		if (cardDeckColor.equals(BLUE)) {
			backImageId = R.drawable.blue_card_back;
		} else if (cardDeckColor.equals(GREEN)) {
			backImageId = R.drawable.green_card_back; 
		} else if (cardDeckColor.equals(RED)) {
			backImageId = R.drawable.red_card_back;
		} else if (cardDeckColor.equals(YELLOW)) {
			backImageId = R.drawable.yellow_card_back;
		}
		frontImageBitmap = decodeSampledBitmapFromResource(getResources(), frontImageId, WIDTH, HEIGHT);
		backImageBitmap = decodeSampledBitmapFromResource(getResources(), backImageId, WIDTH, HEIGHT);
		
	}
	
	private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;
		
		if (height > reqHeight || width > reqWidth) {
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}
		
		return inSampleSize;
	}
	
	private Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {
		final BitmapFactory.Options options = new BitmapFactory.Options();
		
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res,  resId, options);
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
		options.inJustDecodeBounds = false;
		
		return BitmapFactory.decodeResource(res,  resId, options);
	}
	
	private void setImage(Bitmap bmp) { 
		setImageBitmap(bmp);
	}
	
	public void resetOnTouchListener() {
		// Resets the onTouchListener to do nothing
		setOnTouchListener(null);
	}
	
	public void flipCard(boolean faceUp) {		
		cardIsFaceUp = faceUp;
		// Get the new image for the card
		Bitmap cardImage = cardIsFaceUp ? frontImageBitmap : backImageBitmap;
		// Update the ImageView
		setImage(cardImage);
	}
	
	public void randomRotate(int amount) {
		// Randomly rotate the card to make it look more realistic
		int rotation = rand.nextInt(amount) / 2;
		setRotation(rotation);
	}
	
	public void moveTo(float x, float y) {
		// Moves the card to the specified position
		setX(x);
		setY(y);
	}
	
	public void animateTo(float x, float y, int animDuration) {
		/**
		 * Animate the card moving to x, y
		 */
		
		// The cards current position
		float currX = getX();
		float currY = getY();
		// Move the card to the new position
		moveTo(x, y);
		// Create a TranslateAnimation to animate the card moving
		animation = new TranslateAnimation(-(x - currX), 0, -(y - currY), 0);
		animation.setDuration(animDuration);
		animation.setFillAfter(false);
		// Start the animation
		startAnimation(animation);
	}
	
	public void addToLayout(FrameLayout l) {
		// Add the card's ImageView to the table
		l.addView(this);
		// Set dimensions of card to defaults
		getLayoutParams().height = HEIGHT;
		getLayoutParams().width = WIDTH;
	}
	
	public boolean isFaceUp() {
		return cardIsFaceUp;
	}
	
	public String getDeckColor() {
		// Return the color of the cards deck (the back of the card)
		return cardDeckColor;
	}
	
	public String getSuitColor() {
		// Return the suit color of the card (front of the card)
		return cardSuitColor;
	}
	
	public int getCardValue() {
		return cardValue;
	}

	public String toString() {
		String str = "cardSuitColor=" + cardSuitColor
		+ ", cardDeckColor=" + cardDeckColor
		+ ", cardValue=" + cardValue
		+ ", cardIsFaceUp=" + cardIsFaceUp;
		return str;
	}
}