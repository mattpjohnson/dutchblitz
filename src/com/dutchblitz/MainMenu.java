package com.dutchblitz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


public class MainMenu extends Activity {
	
	private Button btnCreateGame;
	private Button btnJoinGame;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_menu);
		
		btnCreateGame = (Button) findViewById(R.id.btnCreateGame);
		btnJoinGame = (Button) findViewById(R.id.btnJoinGame);
		setOnClickListeners();
	}
	
	public void setOnClickListeners() {
		btnCreateGame.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					startGame(v, "create");
				}
		});
		
		btnJoinGame.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				startGame(v, "join");
			}
	});
	}
	
	public void startGame(View v, String joinOrCreate) {
        Intent gameIntent = new Intent(v.getContext(), CardGame.class);
        gameIntent.putExtra("Type", joinOrCreate);
        startActivityForResult(gameIntent, 0);
	}
}